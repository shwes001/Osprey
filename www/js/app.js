angular.module('starter', ['ionic','starter.controllers','ngCordova','ionic-datepicker','ionic.rating','ngFileUpload','ionic-native-transitions'])

.run(function($ionicPlatform,$cordovaDevice,$state, $ionicPopup) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (ionic.Platform.version() > 5.1) {
      var permission7 = window.plugins.permissions;
      permission7.hasPermission(checkPermissionCallback7, null, permission7.ACCESS_NETWORK_STATE);
       
      function checkPermissionCallback7(status) {
        if(!status.hasPermission) {
          var errorCallback = function() {
            alert('network state permission is not turned on');
            ionic.Platform.exitApp();
          }
       
          permission7.requestPermission(function(status) {
            if( !status.hasPermission ) errorCallback();
          }, errorCallback, permission7.ACCESS_NETWORK_STATE);
        }
      };

      var permission9 = window.plugins.permissions;
      permission9.hasPermission(checkPermissionCallback9, null, permission9.WAKE_LOCK);
       
      function checkPermissionCallback9(status) {
        if(!status.hasPermission) {
          var errorCallback = function() {
            alert('wake lock permission is not turned on');
            ionic.Platform.exitApp();
          }
       
          permission9.requestPermission(function(status) {
            if( !status.hasPermission ) errorCallback();
          }, errorCallback, permission9.WAKE_LOCK);
        }
      };

      var permission10 = window.plugins.permissions;
      permission10.hasPermission(checkPermissionCallback10, null, permission10.VIBRATE);
       
      function checkPermissionCallback10(status) {
        if(!status.hasPermission) {
          var errorCallback = function() {
            alert('vibrate permission is not turned on');
            ionic.Platform.exitApp();
          }
       
          permission10.requestPermission(function(status) {
            if( !status.hasPermission ) errorCallback();
          }, errorCallback, permission10.VIBRATE);
        }
      };

      var permission1 = window.plugins.permissions;
      permission1.hasPermission(checkPermissionCallback1, null, permission1.READ_PHONE_STATE);
       
      function checkPermissionCallback1(status) {
        if(!status.hasPermission) {
          var errorCallback = function() {
            alert('device permission is not turned on');
            ionic.Platform.exitApp();
          }
       
          permission1.requestPermission(function(status) {
            if( !status.hasPermission ) errorCallback();
          }, errorCallback, permission1.READ_PHONE_STATE);
        }
      };

      var permission2 = window.plugins.permissions;
      permission2.hasPermission(checkPermissionCallback2, null, permission2.INTERNET);
       
      function checkPermissionCallback2(status) {
        if(!status.hasPermission) {
          var errorCallback = function() {
            alert('internet permission is not turned on');
            ionic.Platform.exitApp();
          }
       
          permission2.requestPermission(function(status) {
            if( !status.hasPermission ) errorCallback();
          }, errorCallback, permission2.INTERNET);
        }
      };

      var permission3 = window.plugins.permissions;
      permission3.hasPermission(checkPermissionCallback3, null, permission3.ACCESS_COARSE_LOCATION);
       
      function checkPermissionCallback3(status) {
        if(!status.hasPermission) {
          var errorCallback = function() {
            alert('coarse permission is not turned on');
            ionic.Platform.exitApp();
          }
       
          permission3.requestPermission(function(status) {
            if( !status.hasPermission ) errorCallback();
          }, errorCallback, permission3.ACCESS_COARSE_LOCATION);
        }
      };

      var permission4 = window.plugins.permissions;
      permission4.hasPermission(checkPermissionCallback4, null, permission4.ACCESS_FINE_LOCATION);
       
      function checkPermissionCallback4(status) {
        if(!status.hasPermission) {
          var errorCallback = function() {
            alert('fine permission is not turned on');
            ionic.Platform.exitApp();
          }
       
          permission4.requestPermission(function(status) {
            if( !status.hasPermission ) errorCallback();
          }, errorCallback, permission4.ACCESS_FINE_LOCATION);
        }
      };

      var permission5 = window.plugins.permissions;
      permission5.hasPermission(checkPermissionCallback5, null, permission5.WRITE_EXTERNAL_STORAGE);
       
      function checkPermissionCallback5(status) {
        if(!status.hasPermission) {
          var errorCallback = function() {
            alert('external storage permission is not turned on');
            ionic.Platform.exitApp();
          }
       
          permission5.requestPermission(function(status) {
            if( !status.hasPermission ) errorCallback();
          }, errorCallback, permission5.WRITE_EXTERNAL_STORAGE);
        }
      }

      var permission6 = window.plugins.permissions;
      permission6.hasPermission(checkPermissionCallback6, null, permission6.RECEIVE_BOOT_COMPLETED);
       
      function checkPermissionCallback6(status) {
        if(!status.hasPermission) {
          var errorCallback = function() {
            alert('boot permission is not turned on');
            ionic.Platform.exitApp();
          }
       
          permission6.requestPermission(function(status) {
            if( !status.hasPermission ) errorCallback();
          }, errorCallback, permission6.RECEIVE_BOOT_COMPLETED);
        }
      };

    } else {
      
    }
  });

  $ionicPlatform.registerBackButtonAction(function (event) {
    if($state.current.name=="app.map" || $state.current.name=="app.register" ){
      navigator.app.exitApp();
    }
    else {
      navigator.app.backHistory();
    }
    event.preventDefault();
    return false;
  }, 101);
})

.filter('monthFormat', function myDateFormat($filter){
  return function(text){
    var  tempdate= new Date(text.replace(/-/g,"/"));
    return $filter('date')(tempdate, "MMM");
  }
})

.filter('dayFormat', function myDateFormat($filter){
  return function(text){
    var  tempdate= new Date(text.replace(/-/g,"/"));
    return $filter('date')(tempdate, "dd");
  }
})

.directive('input', function($timeout) {
  return {
    restrict: 'E',
    scope: {
      'returnClose': '=',
      'onReturn': '&',
      'onFocus': '&',
      'onBlur': '&'
    },
    link: function(scope, element, attr) {
      element.bind('focus', function(e) {
        if (scope.onFocus) {
          $timeout(function() {
            scope.onFocus();
          });
        }
      });
      element.bind('blur', function(e) {
        if (scope.onBlur) {
          $timeout(function() {
            scope.onBlur();
          });
        }
      });
      element.bind('keydown', function(e) {
        if (e.which == 13) {
          if (scope.returnClose) element[0].blur();
          if (scope.onReturn) {
            $timeout(function() {
              scope.onReturn();
            });
          }
        }
      });
    }
  }
})

.config(function($ionicNativeTransitionsProvider){
    $ionicNativeTransitionsProvider.setDefaultTransition({
        "type"          : "slide",
        "direction"     : "left",
        "duration"      :  500
    });
    $ionicNativeTransitionsProvider.setDefaultBackTransition({
        type: 'slide',
        direction: 'right'
    });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'templates/menu.html',
      controller: 'AppCtrl'
    })

    .state('app.tabs', {
      url: "/tabs",
      nativeTransitionsAndroid: {
        "type": "slide",
        "direction": "left"
      },
      views: {
        'menuContent': {
          templateUrl: "templates/Tasks/tabs.html"
        }
      }
    })

    .state('app.tabs.assignedTasks', {
      url: '/assignedTasks',
      nativeTransitionsAndroid: {
        "type": "slide",
        "direction": "left"
      },
      views: {
        'tab-assignedTasks': {
          templateUrl: 'templates/Tasks/assignedTask.html',
          controller: 'assignedTaskCtrl'
        }
      }
    })

    .state('app.tabs.overdueTasks', {
      url: '/overdueTasks',
      nativeTransitionsAndroid: {
        "type": "slide",
        "direction": "left"
      },
      views: {
        'tab-overdueTasks': {
          templateUrl: 'templates/Tasks/overdueTask.html',
          controller: 'overdueTaskCtrl'
        }
      }
    })

    .state('app.tabs.completedTasks', {
      url: '/completedTasks',
      nativeTransitionsAndroid: {
        "type": "slide",
        "direction": "left"
      },
      views: {
        'tab-completedTasks': {
          templateUrl: 'templates/Tasks/completedTask.html',
          controller: 'completedTaskCtrl'
        }
      }
    })

    .state('app.report', {
      url: "/task/:id",
      nativeTransitionsAndroid: {
        "type": "slide",
        "direction": "left"
      },
      views: {
        'menuContent': {
          templateUrl: "templates/Tasks/report.html",
          controller: 'ReportCtrl'
        }
      }
    })

    .state('app.register', {
      url: '/register',
      views: {
        'menuContent': {
          templateUrl: 'templates/login/register.html',
          controller: 'RegisterCtrl'
        }
      }
    })

    .state('app.places', {
      url: '/places',
      nativeTransitionsAndroid: {
        "type": "slide",
        "direction": "left"
      },
      views: {
        'menuContent': {
          templateUrl: 'templates/maps/places.html',
          controller: 'PlacesCtrl'
        }
      }
    })

    .state('app.map', {
      url: '/map',
      nativeTransitionsAndroid: {
       "type": "slide",
        "direction": "left"
      },
      views: {
        'menuContent': {
          templateUrl: 'templates/maps/map.html',
          controller: 'MapCtrl'
        }
      }
    })

    .state('app.individualTask', {
      url: '/individual/:id',
      nativeTransitionsAndroid: {
        "type": "slide",
        "direction": "left"
      },
      views: {
        'menuContent': {
          templateUrl: 'templates/Tasks/individualTask.html',
          controller: 'TaskCtrl'
        }
      }
    })

    .state('app.signup', {
      url: '/signup',
      nativeTransitionsAndroid: {
        "type": "slide",
        "direction": "left"
      },
      views: {
        'menuContent': {
          templateUrl: 'templates/register/signup.html',
          controller: 'SignUpCtrl'
        }
      }
    })

    .state('app.name', {
      url: '/name',
      nativeTransitionsAndroid: {
        "type": "slide",
        "direction": "left"
      },
      views: {
        'menuContent': {
          templateUrl: 'templates/register/name.html',
          controller: 'NameCtrl'
        }
      }
    })

    .state('app.setPassword', {
      url: '/setPassword',
      nativeTransitionsAndroid: {
        "type": "slide",
        "direction": "left"
      },
      views: {
        'menuContent': {
          templateUrl: 'templates/register/setPassword.html',
          controller: 'SetPasswordCtrl'
        }
      }
    })

    .state('app.confirmPassword', {
      url: '/confirmPassword',
      nativeTransitionsAndroid: {
        "type": "slide",
        "direction": "left"
      },
      views: {
        'menuContent': {
          templateUrl: 'templates/register/confirmPassword.html',
          controller: 'ConfirmPasswordCtrl'
        }
      }
    })

    .state('app.login', {
      url: '/login',
      nativeTransitionsAndroid: {
        "type": "slide",
        "direction": "left"
      },
      views: {
        'menuContent': {
          templateUrl: 'templates/login/login.html',
          controller: 'LoginCtrl'
        }
      }
    })

    .state('app.otp', {
      url: '/otp',
      nativeTransitionsAndroid: {
        "type": "slide",
        "direction": "left"
      },
      views: {
        'menuContent': {
          templateUrl: 'templates/login/otp.html',
          controller: 'OtpCtrl'
        }
      }
    })

    .state('app.password', {
      url: '/password',
      nativeTransitionsAndroid: {
        "type": "slide",
        "direction": "left"
      },
      views: {
        'menuContent': {
          templateUrl: 'templates/login/password.html',
          controller: 'PasswordCtrl'
        }
      }
    })

    .state('app.forgotPassword', {
      url: '/forgotPassword',
      nativeTransitionsAndroid: {
        "type": "slide",
        "direction": "left"
      },
      views: {
        'menuContent': {
          templateUrl: 'templates/login/forgotPassword.html',
          controller: 'ForgotPasswordCtrl'
        }
      }
    })

    .state('app.contactUs', {
    url: '/contactUs',
    nativeTransitionsAndroid: {
        "type": "slide",
        "direction": "left"
      },
    views: {
      'menuContent': {
        templateUrl: 'templates/ContactUs/contactUs.html',
        controller: 'ContactCtrl'
      }
    }
  })

    .state('app.traffic', {
    url: '/traffic',
    nativeTransitionsAndroid: {
        "type": "slide",
        "direction": "left"
      },
    views: {
      'menuContent': {
        templateUrl: 'templates/maps/traffic.html',
        controller: 'TrafficCtrl'
      }
    }
  })

    .state('welcome', {
      url: '/welcome',
      nativeTransitionsAndroid: {
        "type": "slide",
        "direction": "left"
      },
      templateUrl: 'templates/login/welcome.html',
      controller: 'WelcomeCtrl'
  })

    .state('app.profile', {
      url: '/profile',
      nativeTransitionsAndroid: {
          "type": "slide",
          "direction": "left"
        },
      views: {
        'menuContent': {
          templateUrl: 'templates/profile/profile.html',
          controller: 'ProfileCtrl'
        }
      }
  })
    .state('app.chats', {
      url: '/chats',
      nativeTransitionsAndroid: {
          "type": "slide",
          "direction": "left"
        },
      views: {
        'menuContent': {
          templateUrl: 'templates/Chats/chats.html',
          controller: 'chatsCtrl'
        }
      }
  })

    .state('app.version', {
      url: "/version",
      nativeTransitionsAndroid: {
        "type": "slide",
        "direction": "left"
      },
      views: {
        'menuContent': {
          templateUrl: "templates/Extras/version.html",
          controller: 'VersionCtrl'
        }
      }
    })

    .state('app.rating', {
      url: "/rating",
      nativeTransitionsAndroid: {
        "type": "slide",
        "direction": "left"
      },
      views: {
        'menuContent': {
          templateUrl: "templates/Extras/rating.html",
          controller: 'RatingCtrl'
        }
      }
    })

    .state('app.terms', {
      url: "/terms",
      nativeTransitionsAndroid: {
        "type": "slide",
        "direction": "left"
      },
      views: {
        'menuContent': {
          templateUrl: "templates/Extras/terms.html",
          controller: 'TermsCtrl'
        }
      }
    })

    .state('app.routes', {
      url: "/routes",
      nativeTransitionsAndroid: {
        "type": "slide",
        "direction": "left"
      },
      views: {
        'menuContent': {
          templateUrl: "templates/routes/route.html",
          controller: 'RouteCtrl'
        }
      }
    });

  $urlRouterProvider.otherwise('/welcome');
});

