angular.module('starter.controllers', [])
.controller('AppCtrl', function($scope,$ionicPopup,$location,$window,$http,
    $ionicHistory,Upload,$cordovaToast,$ionicSideMenuDelegate,$ionicLoading) {
    setInterval(function() {
      document.addEventListener('deviceready', function() {
        if (navigator.getBattery) {
         navigator.getBattery().then(function(battery) {
              var level = Math.round((battery.level)*100);
              $window.localStorage['batteryLevel'] = level;
              $window.localStorage['battery'] = level;
         });

        } else if (navigator.battery || navigator.mozBattery) { 
          var battery = navigator.battery || navigator.mozBattery;
          var level = Math.round(battery._level);
          $window.localStorage['batteryLevel'] = level;
          $window.localStorage['battery'] = level;
        }
      });
    }, 300);

 
  $scope.$on('$ionicView.enter', function (e,config) {
    document.addEventListener('deviceready', function() {
        cordova.plugins.diagnostic.getLocationMode(function(mode){
          if (mode === 'high_accuracy') {   

          } else if (mode === 'location_off'){
            cordova.dialogGPS("Your GPS is Disabled. Enable your location!!!!",
              "Use GPS, with wifi or 3G.",
              function(buttonIndex){
                switch(buttonIndex) {
                  case 0: break;
                  case 1: break;
                }},
                "Please Turn on GPS",
                ["Cancel","Go"]);
          } else if (mode === 'battery_saving') {
              cordova.dialogGPS("Your GPS is in battery saving mode. ",
              "Enable it to High Accuracy!!!!",
              function(buttonIndex){
                switch(buttonIndex) {
                  case 0: break;
                  case 1: break;
                }},
                "Change GPS to High Accuracy",
                ["Cancel","Go"]);
          }else {
            cordova.dialogGPS("Enable your gps to high accuracy mode",
              function(buttonIndex){
                switch(buttonIndex) {
                  case 0: break;
                  case 1: break;
                }},
                "Change GPS to High Accuracy",
                ["Cancel","Go"]);
          }
        });
      });
    $scope.admin = true;
    if ($window.localStorage['userType'] === 'superadmin') {
      $scope.admin = false;
    } else if ($window.localStorage['userType'] === 'admin') {
      $scope.admin = false;
    } else {
      $scope.admin = true;
    }

    $scope.input = {};
    $scope.viewImage = false;
    var data = {};
    data.userId = $window.localStorage['userId'];
    data.apiKey = $window.localStorage['apiKey'];
    var url = "http://192.241.86.4/api/view-user-profile";
    $http({
      method: 'POST',
      url: url,
      crossDomain:true,
      data: data
      }).success(function (response) {
        $scope.email = response[0].email;
        $scope.phone = response[0].phone;
        $scope.firstName = response[0].firstName;
        if (response[0].imageUrl === '' || response[0].imageUrl === undefined) {
          $scope.viewImage = true;
        } else {
          $scope.viewImage = false;
          $scope.imageUrl = response[0].imageUrl;
        }
      },function error(response) {
      });

      var device = {};
      device.apiKey = $window.localStorage['apiKey'];
      device.phone = $window.localStorage['phone'];
      device.companyId = $window.localStorage['companyId'];
      $http({
        method: 'POST',
        url: "http://192.241.86.4/api/fetch-assigned-tasks",
        crossDomain:true,
        data: device
        }).success(function (response) {
          if (response.allTask) {
            $scope.taskLength = response.allTask.length;
          } else {
            $scope.taskLength = 0;
          }
          
        },function error(response) {
          $ionicPopup.alert({
              title: 'Alert',
              template: 'Sorry!! No tasks present!!',
              okText: 'Close'
          });
      });
 
  });

  $scope.upload = function (file) {
      Upload.upload({
          url: 'http://192.241.86.4/api/upload-profile-image',
          method: 'POST',
          file: file,
          sendFieldsAs: 'form',
          fields: {
              userId: $window.localStorage['userId']
          }
      }).then(function (resp) {
          window.location.reload();
      }, function (resp) {
          $ionicPopup.alert({
              title: 'Alert',
              template: 'Error while uploading file ',
              okText: 'Close'
          });
        }, function (evt) {
      });

  };

  $scope.logout = function () {
    $ionicLoading.show({
      template: '<ion-spinner icon="spiral"/>',
      animation: 'fade-in',
      showBackdrop: false,
      showDelay: 0
    });
    var data = {};
    data.userId =  $window.localStorage['userId'];
    data.phone =  $window.localStorage['phone'];
    data.deviceId = $window.localStorage['imei'];
    data.logoutTime = new Date().toUTCString();
    var url = "http://192.241.86.4/api/user-logout";
    $http({
      method: 'POST',
      url: url,
      crossDomain:true,
      data: data
      }).success(function (response) {
        if (response[0].status === 1) {
          $window.localStorage.clear();
          $ionicLoading.hide();
          ionic.Platform.exitApp();
          document.addEventListener('deviceready', function() {
            var bgLocationServices =  window.plugins.backgroundLocationServices;
            bgLocationServices.stop();
          });
        } else {
          $ionicLoading.hide();
          $ionicPopup.alert({
             title: 'Error',
             template: 'Some error occurred. Please try again!!',
             okText: 'Close'
           });
        }
      },function error(response) {
      });
  };

})

.controller('VersionCtrl', function($scope, $timeout, $ionicScrollDelegate, 
                  $window, $http, $ionicPopup) {

  $scope.$on('$ionicView.beforeEnter', function (e,config) {
    config.enableBack = false;
  });

  $scope.$on('$ionicView.enter', function () {
    $scope.percent = $window.localStorage['battery'];
    $scope.switchHelper = function(value) {
        if (value >= 90)
          return 0;
        if (value < 90 && value >= 40)
          return 1;
        if (value < 40)
          return 2;
      };
  });
})

.controller('RatingCtrl', function($scope, $timeout, $ionicScrollDelegate, 
                  $window, $http, $ionicPopup, $location) {
  $scope.$on('$ionicView.beforeEnter', function (e,config) {
    config.enableBack = false;
  });
  $scope.$on('$ionicView.enter', function () {
    $scope.percent = $window.localStorage['battery'];
    $scope.switchHelper = function(value) {
        if (value >= 90)
          return 0;
        if (value < 90 && value >= 40)
          return 1;
        if (value < 40)
          return 2;
      };
    $scope.ratingArr = [{
        value: 1,
        icon: 'ion-ios-star-outline',
        rating: "1"
      }, {
        value: 2,
        icon: 'ion-ios-star-outline',
        rating: "2"
      }, {
        value: 3,
        icon: 'ion-ios-star-outline',
        rating: 3
      }, {
        value: 4,
        icon: 'ion-ios-star-outline',
        rating: "4"
      }, {
        value: 5,
        icon: 'ion-ios-star-outline',
        rating: "5"
      }];

      $scope.setRating = function(rating,val) {
        var rtgs = $scope.ratingArr;
        for (var i = 0; i < rtgs.length; i++) {
          if (i < val) {
            rtgs[i].icon = 'ion-ios-star';
          } else {
            rtgs[i].icon = 'ion-ios-star-outline';
          }
        };
        $scope.rating = rating;
      };

      $scope.submit = function(input) {
          var device = {};
          device.apiKey = $window.localStorage['apiKey'];
          device.phone = $window.localStorage['phone'];
          device.comment = input.comment;
          device.rating = ($scope.rating).toString();
          $http({
            method: 'POST',
            url: "http://192.241.86.4/api/send-review",
            crossDomain:true,
            data: device
            }).success(function (response) {
              if (response[0].status === 1) {
                $location.path('/app/map');
              } else {
                $ionicPopup.alert({
                   title: 'Error',
                   template: 'Some error occurred. Please try again!!',
                   okText: 'Close'
                 });
              }
            },function error(response) {
              $ionicPopup.alert({
                   title: 'Error',
                   template: 'Some error occurred. Please try again!!',
                   okText: 'Close'
                 });
          });
      };
  });
})

.controller('TermsCtrl', function($scope, $timeout, $ionicScrollDelegate, 
                  $window, $http, $ionicPopup) {
  $scope.$on('$ionicView.beforeEnter', function (e,config) {
    config.enableBack = false;
  });
  $scope.$on('$ionicView.enter', function () {
      $scope.percent = $window.localStorage['battery'];
      $scope.switchHelper = function(value) {
          if (value >= 90)
            return 0;
          if (value < 90 && value >= 40)
            return 1;
          if (value < 40)
            return 2;
        };
  });
})

.controller('chatsCtrl', function($scope, $timeout, $ionicScrollDelegate, 
                  $window, $http, $ionicPopup) {
  $scope.$on('$ionicView.beforeEnter', function (e,config) {
    config.enableBack = false;
  });
  
  $scope.messages = [];

  var alternate,isIOS = ionic.Platform.isWebView() && ionic.Platform.isIOS();
  setInterval(function() {
      var device = {};
      device.apiKey = $window.localStorage['apiKey'];
      device.companyId = $window.localStorage['companyId'];
      device.phone = $window.localStorage['phone'];
      $http({
        method: 'POST',
        url: "http://192.241.86.4/api/fetch-messages",
        crossDomain:true,
        data: device
        }).success(function (response) {
          $scope.messages = response[0].messageArray;
          $window.localStorage['adminPhone'] = response[0].messageArray[0].AdminPhone;
        },function error(response) {
      });
    }, 300);
   
   $scope.$on('$ionicView.enter', function () {
      $scope.percent = $window.localStorage['battery'];
      $scope.switchHelper = function(value) {
          if (value >= 90)
            return 0;
          if (value < 90 && value >= 40)
            return 1;
          if (value < 40)
            return 2;
        };
      var device = {};
      device.apiKey = $window.localStorage['apiKey'];
      device.companyId = $window.localStorage['companyId'];
      device.phone = $window.localStorage['phone'];
      $http({
        method: 'POST',
        url: "http://192.241.86.4/api/fetch-messages",
        crossDomain:true,
        data: device
        }).success(function (response) {
          $scope.messages = response[0].messageArray;
          $window.localStorage['adminPhone'] = response[0].messageArray[0].AdminPhone;
          $ionicScrollDelegate.scrollBottom(true);
        },function error(response) {
          $ionicPopup.alert({
             title: 'Error',
             template: 'Some error occurred. Please try again!!',
             okText: 'Close'
           });
      });
      $scope.sendMessage = function() {
        var data = {};
        data.apiKey = $window.localStorage['apiKey'];
        data.userPhone = $window.localStorage['phone'];
        data.companyId = $window.localStorage['companyId'];
        data.message = $scope.message;
        $http({
          method: 'POST',
          url: "http://192.241.86.4/api/send-message",
          crossDomain:true,
          data: data
          }).success(function (response) {
            $scope.message = '';
            $ionicScrollDelegate.scrollBottom(true);
          },function error(response) {
            $ionicPopup.alert({
               title: 'Error',
               template: 'Some error occurred. Please try again!!',
                okText: 'Close'
             });
        });
    };


    $scope.inputUp = function() {
      if (isIOS) $scope.data.keyboardHeight = 216;
      $timeout(function() {
        $ionicScrollDelegate.scrollBottom(true);
      }, 300);
    };

    $scope.inputDown = function() {
      if (isIOS) $scope.data.keyboardHeight = 0;
      $ionicScrollDelegate.resize();
    };
  }); 

})

.controller('RegisterCtrl', function($scope,$cordovaDevice,$ionicSlideBoxDelegate,$cordovaStatusbar,$http,$location,$cordovaToast) {
    $scope.$on('$ionicView.beforeEnter', function (e,config) {
      config.enableBack = false;
    });
    $scope.$on('$ionicView.enter', function (e,config) {
      $scope.setSecondSlide = false;
      $scope.setFirstSlide = true;
      $scope.setThirdSlide = false;
      $scope.setFourthSlide = false;
      $scope.hide = false;
      $scope.slideChanged = function(index) {
        if (index === 1) {
          $scope.setSecondSlide = true;
          $scope.setFirstSlide = false;
          $scope.setThirdSlide = false;
          $scope.setFourthSlide = false;
          $cordovaStatusbar.styleHex('#B33C3C');
        } else if (index === 2) {
          $scope.setThirdSlide = true;
          $scope.setFirstSlide = false;
          $scope.setSecondSlide = false;
          $scope.setFourthSlide = false;
          $cordovaStatusbar.styleHex('#5cb85c');
        } else if (index === 3){
          $scope.setSecondSlide = false;
          $scope.setFirstSlide = true;
          $scope.setThirdSlide = false;
          $scope.setFourthSlide = true;
          $cordovaStatusbar.styleHex('#5bc0de');
        } else {
          $scope.setSecondSlide = false;
          $scope.setFirstSlide = true;
          $scope.setThirdSlide = false;
          $scope.setFourthSlide = false;
          $cordovaStatusbar.styleHex('#337ab7');
        }
      }
      if(window.Connection) {
          if(navigator.connection.type == Connection.NONE) {
              $scope.hide = true;
          } else {
              $scope.hide = false;
          }
        } else {
          $scope.hide = false;
        }
      $scope.login = function() {
        $location.path('/app/login');
      }

      $scope.signup = function() {
        $location.path('/app/signup');
      }
    });
    
    $scope.$on("$ionicView.afterLeave", function () {
       //$ionicSlideBoxDelegate.stop();
    }); 
})

.controller('ProfileCtrl', function($ionicPlatform,$scope,Upload,$scope,$http,$ionicModal,$window,$ionicPopup, $location,$ionicHistory) {
  if(window.StatusBar) {
      if (ionic.Platform.isAndroid()) {
        StatusBar.backgroundColorByHexString("#000");
      } else {
        StatusBar.styleLightContent();
      }
    }
  $scope.$on('$ionicView.beforeEnter', function (e,config) {
    config.enableBack = false;
  });
  $scope.$on('$ionicView.enter', function () {
    $scope.percent = $window.localStorage['battery'];
    $scope.switchHelper = function(value) {
      if (value >= 90)
        return 0;
      if (value < 90 && value >= 40)
        return 1;
      if (value < 40)
        return 2;
    };
    $scope.input = {};
    var data = {};
    $scope.viewImage = false;
    data.userId = $window.localStorage['userId'];
    data.apiKey = $window.localStorage['apiKey'];
    var url = "http://192.241.86.4/api/view-user-profile";
    $http({
      method: 'POST',
      url: url,
      crossDomain:true,
      data: data
      }).success(function (response) {
        $scope.data = response[0];
        if (response[0].status === 0 || response[0].status === 2) {
          var confirmPopup = $ionicPopup.confirm({
             title: 'Error',
             template: 'Some error occurred!! We will keep trying..'
           });
           confirmPopup.then(function(res) {
             if(res) {
               window.location.reload();
             } else {
             }
           });
        } else {
          $scope.firstName = response[0].firstName;
          $scope.input.firstName = response[0].firstName;
          $scope.input.lastName = response[0].lastName;
          $scope.input.email = response[0].email;
          $scope.input.phone = response[0].phone;
          $scope.input.presentAddress = response[0].address1;
          $scope.input.permanentAddress = response[0].Address1;
          $scope.input.department = response[0].departmentName;
          if (response[0].imageUrl === '' || response[0].imageUrl === undefined) {
            $scope.viewImage = true;
          } else {
            $scope.viewImage = false;
            $scope.imageUrl = response[0].imageUrl;
          }
        }
      },function error(response) {
      });
 

      $scope.upload = function (file) {
        Upload.upload({
            url: 'http://192.241.86.4/api/upload-profile-image',
            method: 'POST',
            file: file,
            sendFieldsAs: 'form',
            fields: {
                userId: $window.localStorage['userId']
            }
        }).then(function (resp) {
            window.location.reload();
        }, function (resp) {
            alert('Error while uploading file ');
        }, function (evt) {
        });
    };
    

      $scope.submitUserInfo = function(input) {
        var data = {};
        data.apiKey = $window.localStorage['apiKey'];
        data.userId = $window.localStorage['userId'];
        data.firstName = input.firstName;
        data.lastName = input.lastName;
        data.address1 = input.presentAddress;
        data.address2 = input.permanentAddress;
        var url = "http://192.241.86.4/api/update-user-profile";
        $http({
          method: 'POST',
          url: url,
          crossDomain:true,
          data: data
          }).success(function (response) {
            window.location.reload();
          },function error(response) {
             $ionicPopup.alert({
               title: 'Error',
               template: 'Some error occurred. Please try again!!',
                okText: 'Close'
             });
          });
      };
  });

  $scope.$on("$ionicView.afterLeave", function () {
     $ionicHistory.clearCache();
  }); 
})

.controller('SignUpCtrl', function($scope,$http,$location,$ionicPopup,
            $window,$filter,$cordovaStatusbar,$ionicHistory) {
  
  $scope.$on('$ionicView.beforeEnter', function (e,config) {
    config.enableBack = false;
  });
  $scope.$on('$ionicView.enter', function () {
    $scope.input = {};
    if(window.StatusBar) {
      if (ionic.Platform.isAndroid()) {
        StatusBar.backgroundColorByHexString("#048C94");
      } else {
        StatusBar.styleLightContent();
      }
    }
    $scope.submitted = false;
    $scope.login = function() {
      $location.path('/app/login');
    };

    $scope.signup = function() {
      $location.path('/app/signup');
    };

    $scope.uniqueEmail = function(email) {
        var data = {};
        data.email = email;
        var url = "http://192.241.86.4/api/check-mail";
        $http({
          method: 'POST',
          url: url,
          crossDomain:true,
          data: data
          }).success(function (response) {
            console.log(response);
            if (response[0].status === 0) {
              console.log('correct');
            } else {
              alert('Email is already taken');
              $scope.input.email = '';
            }
          },function error(response) {

          });
    };

    $scope.submitUserInfo = function(isValid,input) {
      if(isValid){
        var data = {};
        data.phone = input.mobile;
        var url = "http://192.241.86.4/api/check-phone";
        $http({
          method: 'POST',
          url: url,
          crossDomain:true,
          data: data
          }).success(function (response) {
            console.log(response);
            if (response[0].status === 0) {
              $window.localStorage['email'] = input.email;
              $window.localStorage['phone'] = input.mobile;
              $location.path('/app/name');
            } else {
              alert('Number is already taken');
              $scope.input.mobile = '';
            }
          },function error(response) {

          });
      } else {
        $scope.submitted = true;
      }
    };
  });
})

.controller('NameCtrl', function($scope,$http,$location,$ionicPopup,
                          $window,$filter,$ionicHistory,$ionicLoading) {
  $scope.$on('$ionicView.beforeEnter', function (e,config) {
    config.enableBack = false;
  });
  $scope.$on('$ionicView.enter', function () {
    if(window.StatusBar) {
      if (ionic.Platform.isAndroid()) {
        StatusBar.backgroundColorByHexString("#048C94");
      } else {
        StatusBar.styleLightContent();
      }
    }
    $scope.submitted = false;
    $scope.login = function() {
      $location.path('/app/login');
    };

    $scope.signup = function() {
      $location.path('/app/signup');
    };
    $scope.date = new Date().toUTCString();
    $scope.submitInfo = function(isValid,input) {
      if(isValid){
        $ionicLoading.show({
          template: '<ion-spinner icon="spiral"/>',
          animation: 'fade-in',
          showBackdrop: false,
          showDelay: 0
        });
        var data = {};
        data.companyName = input.companyName;
        data.firstName = input.firstName;
        data.lastName = input.lastName;
        data.email = $window.localStorage['email'];
        data.phone = $window.localStorage['phone'];
        data.createdDate = $filter('date')($scope.date, 'dd-MM-yyyy');
        var url = "http://192.241.86.4/api/company-registration";
        $http({
          method: 'POST',
          url: url,
          crossDomain:true,
          data: data
          }).success(function (response) {
            if (response.status === 0) {
              $ionicLoading.hide();
              $ionicPopup.alert({
               title: 'Error',
               template: 'Some error occurred. Please try again!!',
                okText: 'Close'
             });
            } else {
              $window.localStorage['companyId'] = response.companyId;
              $window.localStorage['userId'] = response.userId;
              $location.path('/app/setPassword');
              $ionicLoading.hide();
            }
          },function error(response) {

          });
        } else {
          $scope.submitted = true;
        }
    };
  });
})

.controller('SetPasswordCtrl', function($scope,$http,$location,
                              $ionicPopup,$window,$ionicHistory) {
  $scope.$on('$ionicView.beforeEnter', function (e,config) {
    config.enableBack = false;
  });
  $scope.$on('$ionicView.enter', function () {
    if(window.StatusBar) {
      if (ionic.Platform.isAndroid()) {
        StatusBar.backgroundColorByHexString("#A71E1E");
      } else {
        StatusBar.styleLightContent();
      }
    }
    $scope.submitted = false;
    $scope.setPassword = function(isValid,input) {
      if(isValid) {
        $window.localStorage['subdomain'] = input.subdomain;
        $location.path('/app/confirmPassword');
      } else {
        $scope.submitted = true;
      }
    };
  });
})

.controller('ConfirmPasswordCtrl', function($scope,$http,$location,
                          $ionicPopup,$window,$ionicHistory,$ionicLoading) {
  $scope.$on('$ionicView.beforeEnter', function (e,config) {
    config.enableBack = false;
  });
  $scope.$on('$ionicView.enter', function () {
    if(window.StatusBar) {
      if (ionic.Platform.isAndroid()) {
        StatusBar.backgroundColorByHexString("#048C94");
      } else {
        StatusBar.styleLightContent();
      }
    }
    $scope.submitted = false;
    $scope.showError = false;
    $scope.validatePassword = function(password,confirmPassword) {
        if (password != confirmPassword){
          $scope.showError = true;
        } else {
          $scope.showError = false;
        }
     };
    $scope.submit = function(isValid,input) {
      if(isValid) {
        $ionicLoading.show({
          template: '<ion-spinner icon="spiral"/>',
          animation: 'fade-in',
          showBackdrop: false,
          showDelay: 0
        });
        $location.path('/app/confirmPassword');
        var data = {};
        data.companyId = $window.localStorage['companyId'];
        data.userId = $window.localStorage['userId'];
        data.subDomain = $window.localStorage['subdomain'];
        data.password = input.password;
        var url = "http://192.241.86.4/api/set-admin-password";
        $http({
          method: 'POST',
          url: url,
          crossDomain:true,
          data: data
          }).success(function (response) {
            $ionicPopup.alert({
              title: 'Successfully Registered',
              template: 'You have successfully registered the company. Please login to the web to access the same.',
              okText: 'Close'
            });
            setTimeout(function() {
              $location.path('/app/register');
              $ionicLoading.hide();
            }, 500);
          },function error(response) {
             $ionicLoading.hide();
             $ionicPopup.alert({
               title: 'Error',
               template: 'Some error occurred. Please try again!!',
                okText: 'Close'
             });
          });
      } else {
        $scope.submitted = true;
      }
    };
  });
})

.controller('LoginCtrl', function($scope,$location,$http,
            $window,$ionicPopup,$ionicHistory,$ionicLoading) {
  $scope.$on('$ionicView.beforeEnter', function (e,config) {
    config.enableBack = false;
  });
  $scope.$on('$ionicView.enter', function () {
    if(window.StatusBar) {
      if (ionic.Platform.isAndroid()) {
        StatusBar.backgroundColorByHexString("#048C94");
      } else {
        StatusBar.styleLightContent();
      }
    }
    $scope.submitted =false;

    $scope.signup = function() {
      $location.path('/app/signup');
    };

    $scope.generateOtp = function(isValid,input) {
       if (isValid) {
          $ionicLoading.show({
            template: '<ion-spinner icon="spiral"/>',
            animation: 'fade-in',
            showBackdrop: false,
            showDelay: 0
          });
          $window.localStorage['phone'] = input.contactNumber;
          var url = "http://192.241.86.4/api/check-new-user";
          var data = {};
          data.phone = input.contactNumber;
          $http({
            method: 'POST',
            url: url,
            crossDomain:true,
            data: data
            }).success(function (response) {
              if (response[0].status === 1) {
                $location.path('/app/otp');
                $ionicLoading.hide();
              } else if (response[0].status === 2) {
                $location.path('/app/password');
                $ionicLoading.hide();
              } else {
                $ionicLoading.hide();
                $ionicPopup.alert({
                  title: 'Alert',
                  template: 'Number not yet registered!!',
                  okText: 'Close'
                });
              }
            },function error(response) {
            });
       } else {
          $scope.submitted = true;
       }
    };
  });
})

.controller('PasswordCtrl', function($scope,$location,$ionicPopup,$window,
          $http,$ionicHistory,$cordovaToast,$ionicLoading) {
  $scope.$on('$ionicView.beforeEnter', function (e,config) {
    config.enableBack = false;
  });
  $scope.$on('$ionicView.enter', function () {
    if(window.StatusBar) {
      if (ionic.Platform.isAndroid()) {
        StatusBar.backgroundColorByHexString("#B33C3C");
      } else {
        StatusBar.styleLightContent();
      }
    }
    $scope.submitted = false;

    document.addEventListener('deviceready', function() {
      window.plugins.imei.get(
          function(imei) {
            $window.localStorage['imei'] = imei; 
          },
          function() {
          }
        );
    });

    $scope.login = function(isValid,input) {
      if(isValid) {
        $ionicLoading.show({
            template: '<ion-spinner icon="spiral"/>',
            animation: 'fade-in',
            showBackdrop: false,
            showDelay: 0
          });
        var url = "http://192.241.86.4/api/user-login";
        var data = {};
        data.phone = $window.localStorage['phone'];
        data.password = input.password;
        data.deviceId = $window.localStorage['imei'];
        data.loginTime = new Date().toUTCString();
        $http({
          method: 'POST',
          url: url,
          crossDomain:true,
          data: data
          }).success(function (response) {
            if (response[0].status === 1) {
              $window.localStorage['clientId'] = response[0].clientId;
              $window.localStorage['secretKey'] = response[0].secretKey;
              $window.localStorage['userId'] = response[0].userId;
              $window.localStorage['companyId'] = response[0].companyId;
              $window.localStorage['firstName'] = response[0].name;
              $window.localStorage['userType'] = response[0].usertype;
              var authUrl = "http://192.241.86.4/api/auth-api";
              var authData = {};
              authData.phone = $window.localStorage['phone'];
              authData.clientId = response[0].clientId;
              authData.secretKey = response[0].secretKey;
              $http({
                method: 'POST',
                url: authUrl,
                crossDomain:true,
                data: authData
                }).success(function (response) {
                  $window.localStorage['apiKey'] = response[0].apiKey;
                  $location.path('/app/map');
                  $ionicLoading.hide();
                },function error(response) {
                    
                });
            } else {
              $ionicLoading.hide();
              $ionicPopup.alert({
                title: 'Login Failure',
                template: 'Please verify your credentials',
                okText: 'Close'
              });
              $location.path('/app/login');
            }
          },function error(response) {
          });
      } else { 
        $scope.submitted = true;
      }
        
     };
    $scope.forgotPassword = function() {
      $location.path('/app/forgotPassword');
    };
  });
})

.controller('ForgotPasswordCtrl', function($scope,$location,$ionicPopup,$http,$ionicHistory,$window) {
  $scope.$on('$ionicView.beforeEnter', function (e,config) {
    config.enableBack = false;
  });
  $scope.$on('$ionicView.enter', function () {
      $scope.number = $window.localStorage['phone'];
      if(window.StatusBar) {
        if (ionic.Platform.isAndroid()) {
          StatusBar.backgroundColorByHexString("#048C94");
        } else {
          StatusBar.styleLightContent();
        }
      }
      $scope.submitted = false;
      $scope.forgotPassword = function(isValid,number) {
        if(isValid) {
          var url = "http://192.241.86.4/api/forget-password";
          var data = {};
          data.phone = $window.localStorage['phone'];
          $http({
            method: 'POST',
            url: url,
            crossDomain:true,
            data: data
            }).success(function (response) {
              console.log(response);
              if(response[0].status === 0) {
                $ionicPopup.alert({
                  title: 'Alert',
                  template: 'Some error occurred',
                  okText: 'Close'
                });
              } else if (response[0].status === 1) {
                 var template = 'An email has been sent to ' + response[0].email + ' to reset your password';
                 $ionicPopup.alert({
                  title: 'Successful',
                  template: template,
                  okText: 'Close'
                });
                setTimeout(function() {
                  $location.path('/app/register');
                }, 500);
              } else {

              }
            },function error(response) {

            });
          } else {
            $scope.submitted = true;
          }
     };
  });
})

.controller('OtpCtrl', function($scope,$location,$ionicPopup,
            $window,$http,$ionicHistory,$ionicLoading) {
    $scope.$on('$ionicView.beforeEnter', function (e,config) {
      config.enableBack = false;
    });
    $scope.$on('$ionicView.enter', function () {
        if(window.StatusBar) {
          if (ionic.Platform.isAndroid()) {
            StatusBar.backgroundColorByHexString("#A71E1E");
          } else {
            StatusBar.styleLightContent();
          }
        }

       $scope.submitted = false;
       $scope.firstTimeLogin = true;
       $scope.deviceChange = false;
       $scope.validatePassword = function(password,confirmPassword) {
          if (password != confirmPassword){
            $scope.showError = true;
          } else {
            $scope.showError = false;
          }
       };
      $scope.login = function(isValid,input) {
        if(isValid) {
          $ionicLoading.show({
            template: '<ion-spinner icon="spiral"/>',
            animation: 'fade-in',
            showBackdrop: false,
            showDelay: 0
          });
          var url = "http://192.241.86.4/api/set-password";
          var data = {};
          data.phone = $window.localStorage['phone'];
          data.password = input.password;
          $http({
            method: 'POST',
            url: url,
            crossDomain:true,
            data: data
            }).success(function (response) {
              if(response[0].status === 0) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                  title: 'Alert',
                  template: 'Some error occurred',
                  okText: 'Close'
                });
              } else if (response[0].status === 1) {
                $ionicPopup.alert({
                    title: 'Password Set Successfully',
                    template: 'Login with your credentials',
                    okText: 'Ok'
                });
                setTimeout(function() {
                    $location.path('/app/register');
                    $ionicLoading.hide();
                  }, 300);
              } else {
                $ionicLoading.hide();
              }
            },function error(response) {
            });
        } else {
          $scope.submitted = true;
        }
      };
    });
})

.controller('WelcomeCtrl', function($scope,$http,$ionicModal,$ionicPopup,$window, $location,$ionicHistory,$cordovaToast) {
  if(window.StatusBar) {
      if (ionic.Platform.isAndroid()) {
        StatusBar.backgroundColorByHexString("#337ab7");
      } else {
        StatusBar.styleLightContent();
      }
    }

  $scope.$on('$ionicView.beforeEnter', function (e,config) {
    config.enableBack = false;
  });
  setInterval(function() {
      if(window.Connection) {
        if(navigator.connection.type == Connection.NONE) {
            $cordovaToast
              .show('Internet Disconnected!!!', 'long', 'center')
              .then(function(success) {
              }, function (error) {
            });
        }
      }
    }, 20000);
  $scope.$on('$ionicView.enter', function () {
    if(window.Connection) {
        if(navigator.connection.type == Connection.NONE) {
            $ionicPopup.confirm({
                title: "Internet Disconnected",
                content: "The internet is disconnected on your device."
            })
            .then(function(result) {
              $window.localStorage['hide'] = true;
              if(result) {
                 ionic.Platform.exitApp();
              } else {
              }
            });
        }
      }
    if ($window.localStorage['phone'] === undefined) {
      $location.path('/app/register');
    } else {
        var url = "http://192.241.86.4/api/check-login-status";
        var data = {};
        data.phone = $window.localStorage['phone'];
        $http({
        method: 'POST',
        url: url,
        crossDomain:true,
        data: data
        }).success(function (response) {
        if (response[0].status === 1) {
         if (($window.localStorage['imei']) !== (response[0].deviceId)) {
           $location.path('/app/register');
         } else {
            $location.path('/app/map');
         }
          
        } else {
            $location.path('/app/register');
        }
      });
      }
  });
  $scope.$on("$ionicView.afterLeave", function () {
     $ionicHistory.clearCache();
  }); 
  
})

.controller('ReportCtrl', function($ionicPlatform,$scope,$http,$window,$ionicLoading,
            $location,$ionicHistory, Upload, $timeout,$stateParams,$ionicPopup) {
    if(window.StatusBar) {
      if (ionic.Platform.isAndroid()) {
        StatusBar.backgroundColorByHexString("#000");
      } else {
        StatusBar.styleLightContent();
      }
    }
    $scope.$on('$ionicView.enter', function () {
      $scope.percent = $window.localStorage['battery'];
      $scope.switchHelper = function(value) {
        if (value >= 90)
          return 0;
        if (value < 90 && value >= 40)
          return 1;
        if (value < 40)
          return 2;
      };
      var taskId = $stateParams.id;
      $scope.upload = function (file,description) {
          $ionicLoading.show({
            template: '<ion-spinner icon="spiral"/>',
            animation: 'fade-in',
            showBackdrop: false,
            showDelay: 0
          });
          Upload.upload({
              url: 'http://192.241.86.4/api/sent-report',
              method: 'POST',
              file: file,
              sendFieldsAs: 'form',
              fields: {
                  taskId: taskId,
                  taskDescription: description
              }
          }).then(function (resp) {
              $ionicLoading.hide();
              if(resp.data.status === 0) {
                $ionicPopup.alert({
                    title: 'Alert',
                    template: 'Error while uploading file. Please try again later!!!',
                    okText: 'Ok'
                });
              } else if (resp.data.status === 2) {
                $ionicPopup.alert({
                    title: 'Alert',
                    template: 'Some error occurred. Please try again later!!!',
                    okText: 'Ok'
                });
              } else if (resp.data.status === 1) {
                var alertPopup = $ionicPopup.alert({
                                    title: 'Alert',
                                    template: 'Success',
                                    okText: 'Ok'
                                 });
                alertPopup.then(function(res) {
                  $location.path('/app/tabs/completedTasks');
                });
              } else if (resp.data.status === 3) {
                $ionicPopup.alert({
                    title: 'Alert',
                    template: 'File cannot be uploaded!!!',
                    okText: 'Ok'
                });
              } else {
              }
          }, function (resp) {
              $ionicLoading.hide();
              alert('Unable to upload. Please try again later!!!!');
          }, function (evt) {
              
          });
    };
  });

  $scope.$on("$ionicView.afterLeave", function () {
     $ionicHistory.clearCache();
  }); 
})

.controller('assignedTaskCtrl', function($scope,$cordovaPush, $cordovaMedia, $ionicPlatform,$http,$state,$ionicModal,
            $window,$location,$ionicHistory,$filter,$ionicScrollDelegate,$ionicPopup) {
  if(window.StatusBar) {
      if (ionic.Platform.isAndroid()) {
        StatusBar.backgroundColorByHexString("#000");
      } else {
        StatusBar.styleLightContent();
      }
    }
  $scope.$on('$ionicView.beforeEnter', function (e,config) {
    config.enableBack = false;
  });

   $scope.$on('$ionicView.enter', function() {
        
        $scope.percent = $window.localStorage['battery'];
        $scope.switchHelper = function(value) {
          if (value >= 90)
            return 0;
          if (value < 90 && value >= 40)
            return 1;
          if (value < 40)
            return 2;
        };
        $scope.notifications = [];
        var pushNotification;
        var config = {
          'badge': 'true',
          'sound': 'true',
          'alert': 'true',
          "senderID": "545481273274" 
        };
        
        $cordovaPush.register(config).then(function (result) {
            resultFunction();
        }, function (err) {
        });

      function resultFunction() {
        pushNotification = window.plugins.pushNotification;


        window.onNotification = function(e){
          switch(e.event){
            case 'registered':
              if(e.regid.length > 0){
                var device_token = e.regid;
                var data = {};
                data.phone = $window.localStorage['phone'];
                data.deviceId =  e.regid;
                var url = "http://192.241.86.4/api/add-device-id";
                $http({
                  method: 'POST',
                  url: url,
                  crossDomain:true,
                  data: data
                  }).success(function (response) {
                  },function error(response) {
                  });
              } else {
              }
            break;

            case 'message':
            break;

            case 'error':
            break;

          }
        };
        window.errorHandler = function(error){
        }


        pushNotification.register(
          onNotification,
          errorHandler,
          {
            'badge': 'true',
            'sound': 'true',
            'alert': 'true',
            'senderID': '545481273274',
            'ecb': 'onNotification'
          }
        );
      }
        $scope.showDiv = false;
        var url = "http://192.241.86.4/api/check-login-status";
        var data = {};
        data.phone = $window.localStorage['phone'];
        $http({
        method: 'POST',
        url: url,
        crossDomain:true,
        data: data
        }).success(function (response) {
        if (response[0].status === 1) {
         if (($window.localStorage['imei']) !== (response[0].deviceId)) {
           $location.path('/app/register');
         } else {
            $scope.search   = '';     // set the default search/filter term
            $scope.showAssigned = [];
              var device = {};
              device.apiKey = $window.localStorage['apiKey'];
              device.phone = $window.localStorage['phone'];
              device.companyId = $window.localStorage['companyId'];
              $http({
                method: 'POST',
                url: "http://192.241.86.4/api/fetch-assigned-tasks",
                crossDomain:true,
                data: device
                }).success(function (response) {
                  $scope.result = response.allTask;
                  for (var i = 0; i < response.allTask.length; i++) {
                    if(response.allTask[i].taskReportStatus === 0) {
                      $scope.showDiv = true;
                      var givenDate = new Date(response.allTask[i].createdDate);
                      var now = new Date();
                      var date1 = $filter('date')(new Date(), 'HH:mm:ss');
                      var date2 = $filter('date')(givenDate, 'HH:mm:ss');
                      var date3 = $filter('date')(givenDate, 'yyyy-MM-dd');
                      var date4 = $filter('date')(new Date(), 'yyyy-MM-dd');
                      if(date4 > date3) {
                        $scope.showAssigned[response.allTask[i].taskId] = false;
                      } else if (date4 == date3){
                          var diff = now - givenDate;
                          var hh = Math.floor(diff / 1000 / 60 / 60);
                          if (hh >= response.allTask[i].taskCompletionTime) {
                            $scope.showAssigned[response.allTask[i].taskId] = false;
                          } else {
                            $scope.showAssigned[response.allTask[i].taskId] = true;
                          }
                      } else {
                        $scope.showAssigned[response.allTask[i].taskId] = true;
                      }
                    } else {
                    }
                  }
                },function error(response) {
                    $ionicPopup.alert({
                        title: 'Alert',
                        template: 'Sorry!! No tasks assigned!!',
                        okText: 'Close'
                    });
              });
          //fetch individual task
          $scope.navigate = function(id) {
            $window.localStorage['taskId'] = id;
            $state.go("app.individualTask", { id: id });
          };
         }
          
        } else {
            $location.path('/app/register');
        }
      },function error(response) {
      });
  });

  $scope.$on("$ionicView.afterLeave", function () {
       $ionicHistory.clearCache();
  });
})

.controller('completedTaskCtrl', function($scope,$http,$ionicModal,
            $window,$ionicHistory,$state,$location,$ionicScrollDelegate,$ionicPopup) {
  if(window.StatusBar) {
      if (ionic.Platform.isAndroid()) {
        StatusBar.backgroundColorByHexString("#000");
      } else {
        StatusBar.styleLightContent();
      }
    }
  $scope.$on('$ionicView.beforeEnter', function (e,config) {
    config.enableBack = false;
  });
   $scope.$on('$ionicView.enter', function() {
            $scope.percent = $window.localStorage['battery'];
            $scope.switchHelper = function(value) {
              if (value >= 90)
                return 0;
              if (value < 90 && value >= 40)
                return 1;
              if (value < 40)
                return 2;
            };
            $scope.showDiv = false;
            var url = "http://192.241.86.4/api/check-login-status";
            var data = {};
            data.phone = $window.localStorage['phone'];
            $http({
            method: 'POST',
            url: url,
            crossDomain:true,
            data: data
            }).success(function (response) {
            if (response[0].status === 1) {
             if (($window.localStorage['imei']) !== (response[0].deviceId)) {
               $location.path('/app/register');
             } else {
                $scope.search   = '';     // set the default search/filter term

                /*function fetchAssignedTaskData(key) {*/
                  var device = {};
                  device.apiKey = $window.localStorage['apiKey'];
                  device.phone = $window.localStorage['phone'];
                  device.companyId = $window.localStorage['companyId'];
                  $http({
                    method: 'POST',
                    url: "http://192.241.86.4/api/fetch-assigned-tasks",
                    crossDomain:true,
                    data: device
                    }).success(function (response) {
                      $scope.result = response.allTask;
                      for (var i = 0; i < response.allTask.length; i++) {
                        if(response.allTask[i].taskReportStatus === 1) { 
                          $scope.showDiv = true;
                        } else {
                        }
                      }
                    },function error(response) {
                      $ionicPopup.alert({
                          title: 'Alert',
                          template: 'Sorry!! No tasks present!!',
                          okText: 'Close'
                      });
                  });
               /* };*/

                //fetch individual task
                $scope.navigate = function(id) {
                  $window.localStorage['taskId'] = id;
                  $state.go("app.individualTask", { id: id });
                };
                
             }
              
            } else {
                $location.path('/app/register');
            }
          },function error(response) {
          });
  });

  $scope.$on("$ionicView.afterLeave", function () {
       $ionicHistory.clearCache();
  }); 

})

.controller('overdueTaskCtrl', function($scope,$http,$state,$ionicModal,
    $window,$ionicHistory,$location,$ionicScrollDelegate,$ionicPopup) {
  if(window.StatusBar) {
      if (ionic.Platform.isAndroid()) {
        StatusBar.backgroundColorByHexString("#000");
      } else {
        StatusBar.styleLightContent();
      }
    }
  $scope.$on('$ionicView.beforeEnter', function (e,config) {
    config.enableBack = false;
  });
   $scope.$on('$ionicView.enter', function() {
        $scope.percent = $window.localStorage['battery'];
        $scope.switchHelper = function(value) {
          if (value >= 90)
            return 0;
          if (value < 90 && value >= 40)
            return 1;
          if (value < 40)
            return 2;
        };
        $scope.showDiv = false;
        var url = "http://192.241.86.4/api/check-login-status";
        var data = {};
        data.phone = $window.localStorage['phone'];
        $http({
        method: 'POST',
        url: url,
        crossDomain:true,
        data: data
        }).success(function (response) {
        if (response[0].status === 1) {
         if (($window.localStorage['imei']) !== (response[0].deviceId)) {
           $location.path('/app/register');
         } else {
                  $scope.search   = '';
                  var device = {};
                  device.apiKey = $window.localStorage['apiKey'];
                  device.phone = $window.localStorage['phone'];
                  device.companyId = $window.localStorage['companyId'];
                  $http({
                    method: 'POST',
                    url: "http://192.241.86.4/api/fetch-assigned-tasks",
                    crossDomain:true,
                    data: device
                    }).success(function (response) {
                      $scope.result = response.overDueTask;
                      for (var i = 0; i < response.overDueTask.length; i++) {
                        if(response.overDueTask[i].taskReportStatus === 0) { 
                          $scope.showDiv = true;
                        } else {
                        }
                      }
                    },function error(response) {
                      $ionicPopup.alert({
                          title: 'Alert',
                          template: 'Sorry!! No tasks assigned!!',
                          okText: 'Close'
                      });
                  });

                //fetch individual task
                $scope.navigate = function(id) {
                  $window.localStorage['taskId'] = id;
                  $state.go("app.individualTask", { id: id });
                };

         }
          
        } else {
            $location.path('/app/register');
        }
      },function error(response) {
      });
    });

  $scope.$on("$ionicView.afterLeave", function () {
       $ionicHistory.clearCache();
  }); 
})


.controller('ContactCtrl', function($scope,$ionicPopup,$location,$http,$window,$ionicHistory) {
  if(window.StatusBar) {
      if (ionic.Platform.isAndroid()) {
        StatusBar.backgroundColorByHexString("#000");
      } else {
        StatusBar.styleLightContent();
      }
    }
  $scope.$on('$ionicView.beforeEnter', function (e,config) {
    config.enableBack = false;
  });
  $scope.$on('$ionicView.enter', function() {
      $scope.submitted = false;
      $scope.percent = $window.localStorage['battery'];
      $scope.switchHelper = function(value) {
        if (value >= 90)
          return 0;
        if (value < 90 && value >= 40)
          return 1;
        if (value < 40)
          return 2;
      };
      $scope.sendMessage = function(isValid,input) {
        if(isValid) {
          var data = {};
          var url = "http://192.241.86.4/api/contact-us";
          data.name = input.name;
          data.phone = $window.localStorage['phone'];
          data.email = input.email;
          data.message = input.message;
          data.subject = input.subject;
          data.apiKey = $window.localStorage['apiKey'];
          data.createdDate = new Date().toUTCString();
          $http({
              method: 'POST',
              url: url,
              crossDomain:true,
              data: data
              }).success(function (response) {
                if (response[0].status === 1) {
                  $ionicPopup.alert({
                      title: 'Thank You',
                      template: 'We will get back to you !!',
                      okText: 'Close'
                    });
                  setTimeout(function() {
                      $location.path('/app/map');
                  }, 200);
                } else {
                  $ionicPopup.alert({
                    title: 'Alert',
                    template: 'Some error occurred',
                    okText: 'Close'
                  });
                }
              },function error(response) {
              });
        } else {
          $scope.submitted = true;
        }
        
      };
  }); 

  $scope.$on("$ionicView.afterLeave", function () {
       $ionicHistory.clearCache();
  }); 
})

.controller('TrafficCtrl', function($scope,$ionicPopup,$location,$http,$window,$ionicHistory) {
  if(window.StatusBar) {
      if (ionic.Platform.isAndroid()) {
        StatusBar.backgroundColorByHexString("#000");
      } else {
        StatusBar.styleLightContent();
      }
    }
  $scope.$on('$ionicView.beforeEnter', function (e,config) {
    config.enableBack = false;
  });
      $scope.percent = $window.localStorage['battery'];
      $scope.switchHelper = function(value) {
        if (value >= 90)
          return 0;
        if (value < 90 && value >= 40)
          return 1;
        if (value < 40)
          return 2;
      };
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (p) {
          var myLatlng = new google.maps.LatLng(p.coords.latitude, p.coords.longitude);
          var mapOptions = {
            zoom: 15,
            center: myLatlng
          }
          var map = new google.maps.Map(document.getElementById('traffic'), mapOptions);
          var trafficLayer = new google.maps.TrafficLayer();
          trafficLayer.setMap(new google.maps.Map(document.getElementById('traffic'), mapOptions));
        })
      } else {

      }
   
})

.controller('PlacesCtrl', function($scope,$cordovaSQLite,$cordovaGeolocation,
        $http,$filter,$window,$ionicPopup,$ionicLoading) {
  if(window.StatusBar) {
      if (ionic.Platform.isAndroid()) {
        StatusBar.backgroundColorByHexString("#000");
      } else {
        StatusBar.styleLightContent();
      }
    }
  $scope.$on('$ionicView.beforeEnter', function (e,config) {
      config.enableBack = false;
    });
  /*$scope.$on('$ionicView.enter', function() {*/
    $scope.percent = $window.localStorage['battery'];
    $scope.switchHelper = function(value) {
      if (value >= 90)
        return 0;
      if (value < 90 && value >= 40)
        return 1;
      if (value < 40)
        return 2;
    };
    document.addEventListener('deviceready', function() {
      $scope.datepickerObjectEnd = {
          titleLabel: 'Title',
          todayLabel: 'Today', 
          closeLabel: 'Close', 
          setLabel: 'Set',
          setButtonType : 'button-assertive',
          todayButtonType : 'button-assertive', 
          closeButtonType : 'button-assertive', 
          inputDate: new Date(), 
          mondayFirst: true,  
          weekDaysList: weekDaysList, 
          monthList: monthList, 
          templateType: 'popup', 
          showTodayButton: 'true', 
          modalHeaderColor: 'bar-positive', 
          modalFooterColor: 'bar-positive', 
          from: new Date(2012, 1, 1), 
          to: new Date(2018, 8, 25),  
          callback: function (val) {  
            datePickerCallbackEnd(val);
          },
          dateFormat: 'dd-MM-yyyy', 
          closeOnSelect: false, 
        };
      $scope.datepickerObjectStart = {
          titleLabel: 'Title',
          todayLabel: 'Today', 
          closeLabel: 'Close', 
          setLabel: 'Set',
          setButtonType : 'button-assertive',
          todayButtonType : 'button-assertive', 
          closeButtonType : 'button-assertive', 
          inputDate: new Date(), 
          mondayFirst: true,  
          weekDaysList: weekDaysList, 
          monthList: monthList, 
          templateType: 'popup', 
          showTodayButton: 'true', 
          modalHeaderColor: 'bar-positive', 
          modalFooterColor: 'bar-positive', 
          from: new Date(2012, 1, 1), 
          to: new Date(2018, 8, 25),  
          callback: function (val) {  
            datePickerCallbackStart(val);
          },
          dateFormat: 'dd-MM-yyyy', 
          closeOnSelect: false, 
        };
        
        var weekDaysList = ["Sun", "Mon", "Tue", "Wed", "thu", "Fri", "Sat"];
        var monthList = ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
        var datePickerCallbackStart = function (val) {
          if (typeof(val) === 'undefined') {
          } else {
            $scope.datepickerObjectStart.inputDate = val;
          }
        };
        var datePickerCallbackEnd = function (val) {
          if (typeof(val) === 'undefined') {
          } else {
            $scope.datepickerObjectEnd.inputDate = val;
          }
        };
      var marker;

      var options = {timeout: 20000, enableHighAccuracy: true};
      
      $cordovaGeolocation.getCurrentPosition(options).then(function(position){
        var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        var mapOptions = {
          center: latLng,
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
     
        $scope.map = new google.maps.Map(document.getElementById("google-map"), mapOptions);

      $scope.centerOnMe = function() {
        $ionicLoading.show({
          template: '<ion-spinner icon="spiral"/>',
          animation: 'fade-in',
          showBackdrop: false,
          showDelay: 0
        });
        var flightPlanCoordinates = [];
        var data = {};
        data.companyId = $window.localStorage['companyId'];
        data.apiKey = $window.localStorage['apiKey'];
        data.startdate = $filter('date')($scope.datepickerObjectStart.inputDate, 'MM/dd/yyyy');
        data.enddate = $filter('date')($scope.datepickerObjectEnd.inputDate, 'MM/dd/yyyy');
        data.phone = $window.localStorage['phone'];
        $http({
           method: 'POST',
           url: "http://192.241.86.4/api/fetch-track-result",
           crossDomain:true,
           data: data
            }).success(function (response) {
              if (response.length > 0) {
                for (var i = 0; i < response.length; i++) {
                  var responseData = response[i];
                  for (var j = 0; j < responseData.length; j++) {
                    var object = {};
                    object.lat = responseData[j].Latitude;
                    object.lng = responseData[j].Longitude;
                    $http.get('http://maps.googleapis.com/maps/api/geocode/json?latlng='
                        +responseData[0].Latitude+','+responseData[0].Longitude+'&sensor=true').
                      then(function(data){ 
                        $scope.address1 = data.data.results[0].formatted_address;
                    }); 
                    $http.get('http://maps.googleapis.com/maps/api/geocode/json?latlng='
                        +responseData[responseData.length - 1].Latitude+','+responseData[responseData.length - 1].Longitude+'&sensor=true').
                      then(function(data){ 
                        $scope.address2 = data.data.results[0].formatted_address;
                    });
                    flightPlanCoordinates.push(object);
                  }
                }
                var flightPath = new google.maps.Polyline({
                    path: flightPlanCoordinates,
                    geodesic: true,
                    strokeColor: '#49E649',
                    strokeOpacity: 1.0,
                    strokeWeight: 2
                  });

                  var length = flightPlanCoordinates.length - 1;

                  google.maps.event.addListenerOnce($scope.map, 'idle', function(){
                    var marker1 = new google.maps.Marker({
                      position: new google.maps.LatLng(flightPlanCoordinates[0].lat,flightPlanCoordinates[0].lng),
                      map: $scope.map
                    });

                    var infoWindow = new google.maps.InfoWindow({
                        content: $scope.address1
                    });
                   
                    google.maps.event.addListener(marker1, 'click', function () {
                        infoWindow.open($scope.map, marker1);
                    });
                 
                  });

                  google.maps.event.addListenerOnce($scope.map, 'idle', function(){
                    var marker2 = new google.maps.Marker({
                        position: new google.maps.LatLng(flightPlanCoordinates[length].lat,flightPlanCoordinates[length].lng),
                        map: $scope.map
                    });

                    var infoWindow = new google.maps.InfoWindow({
                        content: $scope.address2
                    });
                   
                    google.maps.event.addListener(marker2, 'click', function () {
                        infoWindow.open($scope.map, marker2);
                    });
                 
                  });

                  flightPath.setMap($scope.map);
                  $ionicLoading.hide();
              } else {
                $ionicLoading.hide();
                $ionicPopup.alert({
                  title: 'Alert',
                  template: 'No results found!!',
                  okText: 'Close'
                });
              }
              
          },function error(response) {

          });
       }
      }, function(error){
      });
    });
  /*});*/
  
})

.controller('RouteCtrl', function($scope, $timeout, $ionicScrollDelegate, 
                  $window, $http, $ionicPopup) {
      $scope.$on('$ionicView.beforeEnter', function (e,config) {
        config.enableBack = false;
      });
      
      $scope.percent = $window.localStorage['battery'];
      $scope.switchHelper = function(value) {
        if (value >= 90)
          return 0;
        if (value < 90 && value >= 40)
          return 1;
        if (value < 40)
          return 2;
      };
      $scope.submit = function(input) {
         document.getElementById('panel').innerHTML = "";
         var directionsService = new google.maps.DirectionsService();
         var directionsDisplay = new google.maps.DirectionsRenderer();
    
         var map = new google.maps.Map(document.getElementById('routeMap'), {
           zoom:5,
           mapTypeId: google.maps.MapTypeId.ROADMAP
         });
        
         directionsDisplay.setMap(map);
         directionsDisplay.setPanel(document.getElementById('panel'));
    
         var request = {
           origin: input.address1, 
           destination: input.address2,
           travelMode: google.maps.DirectionsTravelMode.DRIVING
         };
    
         directionsService.route(request, function(response, status) {
           if (status == google.maps.DirectionsStatus.OK) {
             directionsDisplay.setDirections(response);
           }
         });
      };
})

.controller('TaskCtrl', function($scope,$stateParams,$filter,$window,
            $cordovaBatteryStatus,$cordovaDevice,$http,$ionicPopup,$state) {
  $scope.percent = $window.localStorage['battery'];
  $scope.switchHelper = function(value) {
    if (value >= 90)
      return 0;
    if (value < 90 && value >= 40)
      return 1;
    if (value < 40)
      return 2;
  };
  $scope.id = $stateParams.id;
  var details = {};
  details.companyId = $window.localStorage['companyId'];
  details.taskId = $scope.id;
  details.apiKey = $window.localStorage['apiKey'];
  $http({
    method: 'POST',
    url: "http://192.241.86.4/api/fetch-assigned-task",
    crossDomain:true,
    data: details
    }).success(function (response) {
      $scope.taskName = response.taskName;
      $scope.taskDescription = response.taskDescription;
      $scope.taskCompletionTime = response.taskCompletionTime;
      $scope.createdDate = response.createdDate;
      $scope.taskId = $window.localStorage['taskId'];
      $scope.taskReportStatus = response.taskReportStatus;
    },function error(response) {
        $ionicPopup.alert({
            title: 'Alert',
            template: 'Sorry!! No tasks assigned!!',
            okText: 'Close'
        });
    });
   $scope.getTaskId = function(id) {
     $state.go("app.report", { id: id });
   };
})

.controller('MapCtrl', function($scope,$rootScope,$ionicLoading, $state, 
  $cordovaGeolocation,$cordovaSQLite,$filter,$window,$ionicPopup,$ionicHistory,
  $cordovaBatteryStatus,$cordovaDevice,$http,$cordovaToast,$ionicSideMenuDelegate,$ionicPopup) {
    if(window.StatusBar) {
      if (ionic.Platform.isAndroid()) {
        StatusBar.backgroundColorByHexString("#000");
      } else {
        StatusBar.styleLightContent();
      }
    }
    $ionicLoading.show({
      template: '<ion-spinner icon="spiral"/>',
      animation: 'fade-in',
      showBackdrop: false,
      showDelay: 0
    });

    $scope.$on('$ionicView.beforeEnter', function (e,config) {
      config.enableBack = false;
    });
    $scope.resultView = false;
    var db = $cordovaSQLite.openDB("my.db"); //mobile
    var tableName = 'Location';
    $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS "+tableName+" (id INTEGER PRIMARY KEY AUTOINCREMENT, latitude DOUBLE, longitude DOUBLE, date NUMERIC)");
    $scope.percent = $window.localStorage['battery'];
    $scope.switchHelper = function(value) {
      if (value >= 90)
        return 0;
      if (value < 90 && value >= 40)
        return 1;
      if (value < 40)
        return 2;
    };
    $scope.percentage = $window.localStorage['battery'];

    var showToast = function(value,type){
      $cordovaToast
        .show(value, type, 'center')
        .then(function(success) {

        }, function (error) {

        });
    };

    $scope.$on('$ionicView.enter', function(){
      $ionicHistory.clearHistory();
      $ionicSideMenuDelegate.canDragContent(true);
      //fetch tasks
      var assignedData = {};
      assignedData.apiKey = $window.localStorage['apiKey'];
      assignedData.phone = $window.localStorage['phone'];
      assignedData.companyId = $window.localStorage['companyId'];
      $http({
        method: 'POST',
        url: "http://192.241.86.4/api/fetch-assigned-tasks",
        crossDomain:true,
        data: assignedData
        }).success(function (response) {
          //$ionicLoading.hide();
          if (response.allTask === undefined) {
            $scope.resultView = true;
            $scope.text = "You haven't been assigned with any tasks yet!!!";
          } else {
            $scope.resultView = false;
            $scope.result = response.allTask;
          }
          
        },function error(response) {
            $ionicPopup.alert({
                title: 'Alert',
                template: 'Error Occurred',
                okText: 'Close'
            });
      });
    });

    setInterval(function() {
        var assignedData = {};
        assignedData.apiKey = $window.localStorage['apiKey'];
        assignedData.phone = $window.localStorage['phone'];
        assignedData.companyId = $window.localStorage['companyId'];
        $http({
          method: 'POST',
          url: "http://192.241.86.4/api/fetch-assigned-tasks",
          crossDomain:true,
          data: assignedData
          }).success(function (response) {
            if (response.allTask === undefined) {
              $scope.resultView = true;
              $scope.text = "You haven't been assigned with any tasks yet!!!";
            } else {
              $scope.resultView = false;
              $scope.result = response.allTask;
            }
          },function error(response) {
            $ionicPopup.alert({
                title: 'Alert',
                template: 'Error Occurred',
                okText: 'Close'
            });
        });
     }, 2000);

    //fetch individual task
    $scope.navigate = function(id) {
      $window.localStorage['taskId'] = id;
      $state.go("app.individualTask", { id: id });
    };

    document.addEventListener('deviceready', function () {
        var companyId = $window.localStorage['companyId'];
        var delay_second = 5;
        var marker;
        var options = {timeout: 2000, enableHighAccuracy: true};

        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function (p) {
            var myLatlng = new google.maps.LatLng(p.coords.latitude, p.coords.longitude);
            $ionicLoading.hide();
            var bgLocationServices =  window.plugins.backgroundLocationServices;
            var lat = p.coords.latitude;
            var long = p.coords.longitude;
            var mapOptions = {
              zoom: 15,
              center: myLatlng
            }
            var map = new google.maps.Map(document.getElementById('map'), mapOptions);
            google.maps.event.addListenerOnce(map, 'idle', function(){

                marker = new google.maps.Marker({
                    map: map,
                    animation: google.maps.Animation.DROP,
                    position: myLatlng,
                    draggable: true
                }); 
           
            });

            bgLocationServices.configure({
                 desiredAccuracy: 5, 
                 distanceFilter: 1, 
                 debug: true, 
                 interval: 10000,
                 notificationTitle: 'Osprey', 
                 notificationText: 'Background Tracking', 
                 fastestInterval: 10000, 
                 useActivityDetection: true 
            });

            bgLocationServices.registerForLocationUpdates(function(location) {
                 console.log("We got an BG Update" + JSON.stringify(location));
                 insertIntoTable(location.latitude,location.longitude);
                 count++;

                if(count%4 == 0){
                  prepareDataToSend();
                  count = 0;
                } else {
                  console.log('count is' + count);
                }
            }, function(err) {
                 console.log("Error: Didnt get an update", err);
            });
            bgLocationServices.start();
          })

        } else {
          showToast("Could not get location","long");
        }

        var count = 1;

        function getLocation() {
            navigator.geolocation.getCurrentPosition(function (p) {
                if (p.coords.accuracy < 10) {
                  console.log('dont do anything');
                } else if (p.coords.accuracy > 100){
                  console.log('dont do anything');
                } else {
                  console.log('new coordinates test');
                  var new_latitude = p.coords.latitude;
                  var new_longitude = p.coords.longitude;
                  var latlng = new google.maps.LatLng(new_latitude, new_longitude);
                  google.maps.event.addListenerOnce(map, 'idle', function(){

                      marker = new google.maps.Marker({
                          map: map,
                          animation: google.maps.Animation.DROP,
                          position: latlng,
                          draggable: true
                      });
                 
                  });
                  insertIntoTable(new_latitude,new_longitude);
                  count++;
                  if(count%4 == 0){
                    prepareDataToSend();
                    count = 0;
                  } else {
                    console.log('count is ' + count);
                  }
                }
            });
        };
        setInterval(getLocation, delay_second*1000);
    });

    
    function insertIntoTable(latitude,longitude){
        console.log('insert into table fired');
        var query = "INSERT INTO "+tableName+" (latitude, longitude, date) VALUES (?,?,DATETIME('now'))";

        $cordovaSQLite.execute(db, query, [latitude,longitude]).then(function(res) {
            console.log('data success');
        }, function (err) {
            console.log('error occurred while saving ' + err.message);
        });
    };

    function prepareDataToSend(){
      console.log('prepare data to send');
      var locations = [];
      var query = "select * from "+tableName;

      $cordovaSQLite.execute(db, query).then(function(result) {
        if (result.rows.length > 0) {
          for (var i=0; i < result.rows.length; i++) {
            var object = {};
            object.deviceId = $window.localStorage['imei'];
            object.deviceName = $cordovaDevice.getModel();
            object.phone = $window.localStorage['phone'];
            object.companyId = $window.localStorage['companyId'];
            object.createdDate = result.rows.item(i).date;
            object.batteryPercentage = $scope.percent;
            object.latitude = result.rows.item(i).latitude;
            object.longitude = result.rows.item(i).longitude;
            object.manual = "0";
            locations.push(object);
          }
          var location = {"locations":locations,"apiKey":$window.localStorage['apiKey']};
          sendDataToServer(location);
        }
      });

    };

    function sendDataToServer(location){
      console.log('send data to server');
      $http({
         method: 'POST',
         url: "http://192.241.86.4/api/save-logs",
         crossDomain:true,
         data: location

        }).success(function (response) {
          console.log('successfully send data');
          clearDb();
        },function error(response) {

        });
    };

    function clearDb(){
        var deleteQuery = "DELETE FROM "+tableName;

        $cordovaSQLite.execute(db, deleteQuery).then(function(res) {
            console.log('db cleared');
        }, function (err) {

        });
    };

});

